/**
 * Created by Dmitry on 7/17/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class ImageViewer extends JFrame {
    private JPanel imagePanel;
    private JPanel buttonPanel;
    private JLabel imageLabel;
    private JButton button;
    private JFileChooser fileChooser;

    public ImageViewer() {
        this.setTitle("Image Viewer");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        buildImagePanel();
        buildButtonPanel();

        this.add(imagePanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);

        fileChooser = new JFileChooser(".");
        this.pack();
        this.setVisible(true);
    }

    private void buildImagePanel() {
        imagePanel = new JPanel();
        imageLabel = new JLabel("Click the button to get an image.");
        imagePanel.add(imageLabel);
    }

    private void buildButtonPanel() {
        buttonPanel = new JPanel();

        button = new JButton("Get Image");
        button.setMnemonic(KeyEvent.VK_G);
        button.setToolTipText("Click here to load an image.");
        button.addActionListener(new ButtonListener());

        buttonPanel.add(button);
    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            File selectedFile;
            ImageIcon image;
            String filename;
            int fileChooserStatus;

            fileChooserStatus = fileChooser.showOpenDialog(ImageViewer.this);

            if (fileChooserStatus == JFileChooser.APPROVE_OPTION) {
                selectedFile = fileChooser.getSelectedFile();
                filename = selectedFile.getPath();
                image = new ImageIcon(filename);
                imageLabel.setIcon(image);
                imageLabel.setText(null);
                pack();
            }
        }
    }

    public static void main(String[] args) {
        new ImageViewer();
    }
}
